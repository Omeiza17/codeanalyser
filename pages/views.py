from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

from pages.fileReader import handle_uploaded_file
from pages.processor import analyse
from .forms import UploadFileForm


@login_required
def home(request):
    uploaded_file = UploadFileForm
    return render(request, 'home.html', {'upload_form': uploaded_file})


@login_required
def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            code = handle_uploaded_file(request.FILES['file'])
            report = analyse(code)
            return render(request, 'feedback.html', {'report': report, 'code': code})
    else:
        return HttpResponseRedirect('')
